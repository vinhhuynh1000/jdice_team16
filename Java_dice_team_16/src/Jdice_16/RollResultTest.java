package Jdice_16;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RollResultTest {
	RollResult rr1, rr2;
	@Before
	public void setUp() throws Exception {
		rr1 = new RollResult(5);
		rr2 = new RollResult(15);
	}
	
	@Test
	public void test() {
		assertEquals("20  <= []+20", rr1.andThen(rr2).toString());
	}

	@After
	public void tearDown() throws Exception {
	}
}
