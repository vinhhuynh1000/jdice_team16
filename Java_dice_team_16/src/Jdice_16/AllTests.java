package Jdice_16;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DiceParserTest.class, DiceRollTest.class, DiceSumTest.class, RollResultTest.class })
public class AllTests {

}
