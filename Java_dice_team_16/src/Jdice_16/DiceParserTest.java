package Jdice_16;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DiceParserTest {
	DiceParser dp;
	@Before
	public void setUp() throws Exception {
		dp = new DiceParser();
	}
	
	@Test
	public void test() {
		assertEquals("[1d4]", dp.parseRoll("d4").toString());
	}

	@After
	public void tearDown() throws Exception {
	}
}
