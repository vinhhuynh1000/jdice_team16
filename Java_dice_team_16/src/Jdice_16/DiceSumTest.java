package Jdice_16;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DiceSumTest {
	DiceSum ds;
	DiceRoll dr1, dr2;
	@Before
	public void setUp() throws Exception {
		dr1 = new DiceRoll(1, 1, 1);
		dr2 = new DiceRoll(2, 2, 2);
		ds = new DiceSum(dr1, dr2);
	}
	
	@Test
	public void test() {
		assertEquals("1d1+1 & 2d2+2", ds.toString());
	}

	@After
	public void tearDown() throws Exception {
	}
}
