package Jdice_16;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DiceRollTest {
	DiceRoll dr;
	@Before
	public void setUp() throws Exception {
		dr = new DiceRoll(1,1,1);
	}
	
	@Test
	public void test() {
		assertEquals("1d1+1", dr.toString());
	}

	@After
	public void tearDown() throws Exception {
	}
}
